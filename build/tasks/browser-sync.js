// Create a Browsersync instance
var bs = require("browser-sync").create();

// Listen to change events on HTML and reload
bs.watch("*.html").on("change", bs.reload);

bs.watch("dist/app.bundle.js").on("change", bs.reload);
// Provide a callback to capture ALL events to CSS
// files - then filter for 'change' and reload all
// css files on the page.
bs.watch("sass/*.scss", function (event, file) {
    if (event === "change") {
        bs.reload("*.scss");
    }
});

// Now init the Browsersync server
bs.init({
	baseDir: "../../",
	server: "./",
	logLevel: "info",
	logPrefix: "Simple Budget",
	logFileChanges: true,
	open: "local",
	startPath: "index.html"
});
