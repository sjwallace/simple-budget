# Simple Budget

Simple Budget is a playground for ES6, React, Redux, Webpack, Karma + Jasmine... 

The goal being that you can get up and running quickly with this boilerplate setup and experiment with some new tech

### Installation

To install the stable version:

```
npm install
```

To run the application

```
npm start
```

To run the unit tests

```
npm test
```

That’s it!


### License

MIT