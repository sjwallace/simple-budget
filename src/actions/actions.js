import * as CONSTANTS from '../constants/constants'

let expenseId = 0;

export function addExpense(expenseObj) {
	return {
		type: CONSTANTS.ADD_EXPENSE,
		payload: expenseObj,
		id: expenseId++
	}
}

export function addCategory(category) {
	return {
		type: CONSTANTS.ADD_CATEGORY,
		payload: category
	}
}

export function selectCategory(id) {
	return {
		type: CONSTANTS.SELECT_CATEGORY,
		payload: id
	}
}

export function selectSubCategory(id) {
	return {
		type: CONSTANTS.SELECT_SUBCATEGORY,
		payload: id
	}
}


export function addBudget() {
	return {
		type: CONSTANTS.ADD_BUDGET
	}
}
