import React, { Component, PropTypes } from 'react';

const propTypes = {
	children: PropTypes.object,
	type: PropTypes.string,
	label: PropTypes.string,
	name: PropTypes.string.isRequired,
	placeholder: PropTypes.string,
	value: PropTypes.oneOfType([
		React.PropTypes.string,
		React.PropTypes.number
	]),
	onBlur: PropTypes.func
}

const defaultProps = {
	type: 'text',
	value: null,
	onBlur: () => {}
}

class InputFormField extends Component {

	constructor() {
		super()
		this.onBlur = this.onBlur.bind(this)
	}

	onBlur(evt) {
		let e = evt || event;
		(typeof this.props.onBlur === 'function') &&
		(this.props.onBlur(e.target.name, e.target.value))
	}

	render() {
		return (
			<div>
				{/*}<label>{this.props.label ? this.props.label : this.props.name}</label>*/}
				<input
					placeholder={this.props.placeholder}
					onBlur={this.onBlur}
					name={this.props.name}
					type={this.props.type}
					value={this.props.value}
					step={this.props.type === 'number' ? 'any' : false}
				/>
				{this.props.children}
			</div>
		)
	}
}

InputFormField.propTypes = propTypes
InputFormField.defaultProps = defaultProps

export default InputFormField
