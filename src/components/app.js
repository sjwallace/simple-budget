import React, { Component, PropTypes } from 'react'

class App extends Component {
	render() {
		return (
			<div>
				<h1>Budget Summary</h1>
				{this.props.children}
			</div>
		)
	}
}

App.propTypes = {
	children: PropTypes.object
}

export default App
