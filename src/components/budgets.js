import React, { Component } from 'react'
import BudgetContainer from '../containers/budgetContainer'
import ExpenseListContainer from '../containers/ExpenseListContainer'
import ExpenseSummaryContainer from '../containers/ExpenseSummaryContainer'

class Budgets extends Component {
	render() {
		return (
			<div>
				<BudgetContainer />
				<ExpenseListContainer />
				<ExpenseSummaryContainer />
			</div>
		)
	}
}

export default Budgets
