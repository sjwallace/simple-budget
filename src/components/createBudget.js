import React, { Component, PropTypes } from 'react'
import AddExpenseContainer from '../containers/addExpenseContainer'

const defaultProps = {
	onAddBudget: () => {},
	isEditingBudget: false
}

const propTypes = {
	onAddBudget: PropTypes.func.isRequired,
	isEditingBudget: PropTypes.bool.isRequired
}

class AddBudget extends Component {

	constructor(props) {
		super(props)
		this.onAddBudget = this.onAddBudget.bind(this)
	}

	onAddBudget() {
		// show categories
		this.props.onAddBudget()
	}

	render() {
		return (
			<div>
				<button
					onClick={this.onAddBudget}
					disabled={this.props.isEditingBudget}
				>
					+ Add a budget
				</button>
				{this.props.isEditingBudget ? <AddExpenseContainer /> : null}
			</div>
		)
	}
}

AddBudget.propTypes = propTypes
AddBudget.defaultProps = defaultProps

export default AddBudget
