import React, { Component } from 'react'
import ExpenseListContainer from '../containers/expenseListContainer'
import ExpenseSummaryContainer from '../containers/expenseSummaryContainer'
import AddExpenseContainer from '../containers/AddExpenseContainer'

class Dashboard extends Component {
	render() {
		return (
			<div>
				<AddExpenseContainer />
				<ExpenseListContainer />
				<ExpenseSummaryContainer />
			</div>
		)
	}
}

export default Dashboard
