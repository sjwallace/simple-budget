import React, { PropTypes } from 'react'

const propTypes = {
	onClick: PropTypes.func.isRequired,
	amount: PropTypes.string.isRequired,
	category: PropTypes.object.isRequired,
	subcategory: PropTypes.object.isRequired
}

const defaultProps = {
	onClick: () => {},
	amount: null,
	category: {},
	subcategory: {}
}

const Expense = ({ onClick, amount, category, subcategory }) => (
	<li
		onClick={onClick}
	>
		{category.name} / {subcategory.name} {amount}
	</li>
)

Expense.propTypes = propTypes
Expense.defaultProps = defaultProps

export default Expense
