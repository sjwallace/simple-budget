import React, { PropTypes } from 'react'
import Expense from './Expense'

const propTypes = {
	expenses: PropTypes.arrayOf(PropTypes.shape({
		id: PropTypes.number.isRequired,
		amount: React.PropTypes.string,
		category: PropTypes.number.isRequired,
		subcategory: PropTypes.number.isRequired
	}).isRequired).isRequired,
	categories: PropTypes.array,
	subcategories: PropTypes.array
}

const defaultProps = {
	expenses: [],
	categories: [],
	subcategories: []
}

const ExpenseList = ({ expenses, categories, subcategories }) => {
	return (
		<ul>
			{expenses.map(expense =>
				<Expense
					amount={expense.amount}
					category={categories.find(v => v.id === expense.category)}
					subcategory={subcategories.find(v => v.id === expense.subcategory)}
					key={expense.id}
				/>
			)}
		</ul>
	)
}

ExpenseList.propTypes = propTypes
ExpenseList.defaultProps = defaultProps

export default ExpenseList
