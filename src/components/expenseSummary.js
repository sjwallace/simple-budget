import React, { PropTypes } from 'react'

const propTypes = {
	total: PropTypes.oneOfType([
		React.PropTypes.string,
		React.PropTypes.number
	])
}

const defaultProps = {
	total: 0
}

const ExpenseSummary = ({ total }) => (
	<h3>
		Total Expenditures: {total}
	</h3>
)

ExpenseSummary.propTypes = propTypes
ExpenseSummary.defaultProps = defaultProps

export default ExpenseSummary
