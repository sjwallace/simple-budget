import React, { Component, PropTypes } from 'react'
import InputFormField from './lib/inputField/inputFormField'
import AddCategory from './addCategory'
import { Map } from 'immutable'

const defaultProps = {
	onSubmitExpense:  () => {},
	onCategorySelection: () => {},
	onSubCategorySelection: () => {},
	categories: [],
	subcategories: [],
	selectedCategory: null,
	selectedSubCategory: null
}

const propTypes = {
	onSubmitExpense: PropTypes.func.isRequired,
	categories: PropTypes.array.isRequired,
	subcategories: PropTypes.array.isRequired,
	onCategorySelection: PropTypes.func.isRequired,
	onSubCategorySelection: PropTypes.func.isRequired,
	selectedCategory: PropTypes.oneOfType([
		React.PropTypes.bool,
		React.PropTypes.number
	]),
	selectedSubCategory: PropTypes.oneOfType([
		React.PropTypes.bool,
		React.PropTypes.number
	])
}

class AddExpense extends Component {

	constructor(props) {

		super(props)

		this.defaultState = [
			['amount', {value: null, isValid: true, type: 'number'}]
		]

		// setup default component state
		this.state = {data: Map(this.defaultState)}

		// bind callback functions to avoid binding within each components event handler
		this.onBlur = this.onBlur.bind(this)
		this.onSubmitExpense = this.onSubmitExpense.bind(this)
	}

	onBlur(fieldKey, value) {

		let newState = this.state.data

		if (this.isEmpty(value)) return

		// use assign here so that the default state objects are not mutated
		this.setState({
			data: newState.set(fieldKey, Object.assign({}, newState.get(fieldKey), { value }))
		})
	}

	onSubmitExpense() {

		if (this.validate(this.state.data)) {
			let newState = this.transform(this.state.data)
			// add the additional properties to the payload
			newState.category = this.props.selectedCategory
			newState.subcategory = this.props.selectedSubCategory
			// dispatch add expense event
			this.props.onSubmitExpense(newState)
			// reset to intial state
			this.setState({
				data: Map(this.defaultState)
			})

		} else {
			// log validation error
		}
	}

	validate(obj) {

		for (let instance of obj.values()) {
			if(this.isEmpty(instance.value)) return false
		}

		return true;
	}

	isEmpty(value) {
		let trimmed = typeof value === 'string' ? value.trim() : value;
		return (trimmed === null || trimmed === '')
	}

	transform(obj) {
		// since we only care about the values for each key in the map
		// we only want to send a minimum payload via our dispatch calls
		let newObj = {}
		for (let [key, val] of obj) {
			newObj[key] = val.value
		}

		return newObj
	}

	renderFormFields() {

		let formFields = []
		let index = 0

		this.state.data.forEach((input, key) => {

			formFields.push(
				<InputFormField
					key={index++}
					name={key}
					type={input.type}
					value={input.value}
					valid={input.isValid}
					onBlur={this.onBlur}
					placeholder={key}
			/>
			)
		})

		return formFields
	}

	render() {
		return (
			<div>
				<AddCategory
					categories={this.props.categories}
					selectedCategory={this.props.selectedCategory}
					onCategorySelection={this.props.onCategorySelection}
				/>
				{this.props.selectedCategory ?
					<AddCategory
						categories={this.props.subcategories}
						selectedCategory={this.props.selectedSubCategory}
						onCategorySelection={this.props.onSubCategorySelection}
					/> : null
				}
				{this.renderFormFields()}
				<button
					className="btn"
					onClick={this.onSubmitExpense}>
					Add Expense
				</button>
			</div>
		)
	}
}

AddExpense.propTypes = propTypes
AddExpense.defaultProps = defaultProps

export default AddExpense
