import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import buttonStyles from './lib/button/buttons.scss'

const defaultProps = {
	onCategorySelection:  () => {},
	categories: [],
	selectedCategory: null
}

const propTypes = {
	onCategorySelection: PropTypes.func.isRequired,
	categories: PropTypes.array.isRequired,
	selectedCategory: PropTypes.oneOfType([
		React.PropTypes.bool,
		React.PropTypes.number
	])
}

class AddCategory extends Component {

	constructor(props) {

		super(props)
		this.onCategorySelection = this.onCategorySelection.bind(this)
	}

	onCategorySelection(id) {

		return () => {
			this.props.onCategorySelection({ id })
		}
	}

	renderCategories() {

		return this.props.categories.map(category => {

			let classes = {
				btn: true,
				tags: true,
				selected: this.props.selectedCategory === category.id
			}

			return (
				<button
					key={category.id}
					className={classNames(classes)}
					onClick={this.onCategorySelection(category.id)}
				>
					{category.name}
				</button>
			)
		}, this)
	}

	render() {
		return (
			<div>
				{this.renderCategories()}
			</div>
		)
	}
}

AddCategory.propTypes = propTypes
AddCategory.defaultProps = defaultProps

export default AddCategory
