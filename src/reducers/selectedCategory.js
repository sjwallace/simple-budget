import { SELECT_CATEGORY, ADD_EXPENSE } from '../constants/constants'

export default function categories(state, action) {
	switch (action.type) {
		case SELECT_CATEGORY:
			return action.payload.id
			break
		case ADD_EXPENSE:
			return false
			break
		default:
			return state
	}
}
