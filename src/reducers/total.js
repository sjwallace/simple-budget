const addExpenses = (prev, next) => {
	return {amount: +prev.amount + +next.amount}
}

export default function total(state = [], action) {
	switch (action.type) {
		case 'ADD_EXPENSE':
			return [...state, action.payload].reduce(addExpenses).amount
		default:
			return 0
	}
}
