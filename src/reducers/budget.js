import { ADD_BUDGET, ADD_EXPENSE } from '../constants/constants'

export default function budget(state = false, action) {
	switch (action.type) {
		case ADD_BUDGET:
			return true
		case ADD_EXPENSE:
			return false
		default:
			return state
	}
}
