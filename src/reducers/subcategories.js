import { ADD_SUBCATEGORY } from '../constants/constants'

// will come from api
const initalState = [
	{
		name: 'Restaurants',
		id: 1
	},
	{
		name: 'Groceries',
		id: 2
	},
	{
		name: 'Water',
		id: 3
	},
	{
		name: 'Power',
		id: 4
	}
]

export default function subcategories(state = initalState, action) {
	switch (action.type) {
		case ADD_SUBCATEGORY:
			return [...state, category]
			break;
		default:
			return state
	}
}
