import budget from './budget'
import total from './total'
import categories from './categories'
import selectedCategory from './selectedCategory'
import subcategories from './subcategories'
import selectedSubCategory from './selectedSubCategory'
import expenses from './expenses'

function budgetApp(state = {}, action) {

	return {
		categories: categories(state.categories, action),
		selectedCategory: selectedCategory(state.selectedCategory, action),
		subcategories: subcategories(state.subcategories, action),
		selectedSubCategory: selectedSubCategory(state.selectedSubCategory, action),
		expenses: expenses(state.expenses, action),
		total: total(state.expenses, action),
		isEditingBudget: budget(state.isEditingBudget, action)
	}
}

export default budgetApp
