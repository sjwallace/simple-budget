import { SELECT_SUBCATEGORY, ADD_EXPENSE } from '../constants/constants'

export default function categories(state, action) {
	switch (action.type) {
		case SELECT_SUBCATEGORY:
			return action.payload.id
			break
		case ADD_EXPENSE:
			return false
			break
		default:
			return state
	}
}
