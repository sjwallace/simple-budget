import { ADD_EXPENSE } from '../constants/constants'

const expense = function(state, action) {
	switch(action.type) {
		case ADD_EXPENSE:
			return Object.assign({}, {id: action.id}, action.payload)
			break;
		default:
			return state
	}
}

export default function budget(state = [], action) {
	switch (action.type) {
		case ADD_EXPENSE:
			return [...state, expense(state, action)]
		default:
			return state
	}
}
