import React from 'react'
import ReactDOM from 'react-dom'
import AddExpense from '../components/addExpense'
import TestUtils from 'react-addons-test-utils'

describe('AddExpense component',() => {

	let node
	let component
	let onSubmit = data => data

	beforeEach(() => {
		component = TestUtils.renderIntoDocument(<AddExpense onSubmitExpense={onSubmit}/>)
		node = ReactDOM.findDOMNode(component)
	})

	afterEach(() => {
		ReactDOM.unmountComponentAtNode(node.parentNode)
		component = null
	})

	it('should render', () => {
		expect(component).toBeDefined()
		expect(node).not.toBe(null)
	})

	it('should render an input field for each key specified in the state', () => {
		let inputs = node.querySelectorAll('input')
		expect(inputs.length).toEqual(component.state.data.size)
	})

	describe('Setting component state on blur event',() => {

		let inputs, setStateSpy

		beforeEach(() => {
			inputs = node.querySelectorAll('input')
			setStateSpy = spyOn(component, 'setState').and.callThrough()
		})

		it('should set the state when valid data is entered and the input instance receives the onBlur event', () => {
			TestUtils.Simulate.blur(inputs[0], {target: {name: 'amount', value: 12}})
			expect(component.state.data.get('amount').value).toEqual(12)
			expect(setStateSpy).toHaveBeenCalled()
		})

		it('should not set the state when invalid data is entered and the input instance receives the onBlur event', () => {
			TestUtils.Simulate.blur(inputs[0], {target: {name: 'amount', value: ""}})
			expect(component.state.data.get('amount').value).toEqual(null)
			expect(setStateSpy).not.toHaveBeenCalled()
		})
	})

	describe('Validating user input',() => {

		it('should return falsy when a value is null', () => {
			expect(component.isEmpty(null)).toEqual(true)
		})

		it('should return falsy when a value is an empty string', () => {
			expect(component.isEmpty('')).toEqual(true)
		})

		it('should return truthy when a value is not null', () => {
			expect(component.isEmpty('foo')).toEqual(false)
		})

		it('should return truthy when a value is not an empty string', () => {
			expect(component.isEmpty(23)).toEqual(false)
		})
	})

	describe('Validating add expense form on submit', () => {

		it('should return falsy when a value of a Map is null', () => {
			let stub = new Map([['test', {value: null, isValid: true}]])
			expect(component.validate(stub)).toEqual(false)
		})

		it('should return falsy when a value of a Map is null', () => {
			let stub = new Map([['test', {value: '', isValid: true}]])
			expect(component.validate(stub)).toEqual(false)
		})

		it('should return truthy when a value of a Map is not null or an empty string', () => {
			let stub = new Map([['test', {value: 'test', isValid: true}]])
			expect(component.validate(stub)).toEqual(true)
		})

		describe('It should return as soon as a vaulue in the map is falsy', () => {

			let stub

			beforeEach(() => {
				stub = new Map([
					['test1', {value: 'test'}],
					['test2', {value: ''}],
					['test3', {value: 'test'}]
				])
			})

			it('should return after 2 calls when the second value is falsy', () => {
				let validationSpy = spyOn(component, 'isEmpty').and.callThrough()
				component.validate(stub)
				expect(validationSpy).toHaveBeenCalledTimes(2)
			})

			it('should return after 1 call when the first value is falsy', () => {
				let validationSpy = spyOn(component, 'isEmpty').and.callThrough()
				stub.set('test1', {value: ''})
				component.validate(stub)
				expect(validationSpy).toHaveBeenCalledTimes(1)
			})
		})
	})

	describe('User input preparation on form submit', () => {

		let stub

		beforeAll(() => {
			stub = new Map([
				['test1', {value: 1}],
				['test2', {value: 2}],
				['test3', {value: 3}]
			])
		})

		it('should return a simple object when after transform has been called on the map', () => {
			let result = component.transform(stub)

			expect(result).toBeDefined()
			expect(Object.keys(result).length).toEqual(3)
			expect(result.test1).toEqual(1)
			expect(result.test2).toEqual(2)
			expect(result.test3).toEqual(3)
		})
	})
})
