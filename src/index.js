import React from 'react'
import App from './components/app'
import Dashboard from './components/dashboard'
import Budgets from './components/budgets'
import BudgetApp from './reducers'
import { render } from 'react-dom'
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import { Provider } from 'react-redux'
import { createStore } from 'redux'

let store = createStore(BudgetApp)

render((
	<Provider store={store}>
		<Router history={browserHistory}>
			<Route
				path="/"
				component={App}
			>
				<IndexRoute component={Dashboard} />
				<Route
					path="/budget"
					component={Budgets}
				/>
			</Route>
		</Router>
	</Provider>
), document.getElementById('app'))
