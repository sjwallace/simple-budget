import React from 'react'
import { connect } from 'react-redux'
import {
	addExpense,
	addCategory,
	selectCategory,
	selectSubCategory
} from '../actions/actions'
import AddExpense from '../components/addExpense'

const mapStateToProps = (state) => {
	return {
		categories: state.categories,
		selectedCategory: state.selectedCategory,
		subcategories: state.subcategories,
		selectedSubCategory: state.selectedSubCategory
	}
}

const mapDispatchToProps = (dispatch) => {
	return {

		onSubmitExpense(payload) {
			dispatch(addExpense(payload))
		},

		onCategoryAddition(payload) {
			dispatch(addCategory(payload))
		},

		onCategorySelection(payload) {
			dispatch(selectCategory(payload))
		},

		onSubCategoryAddition(payload) {
			dispatch(addSubCategory(payload))
		},

		onSubCategorySelection(payload) {
			dispatch(selectSubCategory(payload))
		}
	}
}

const AddExpenseContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(AddExpense)

export default AddExpenseContainer
