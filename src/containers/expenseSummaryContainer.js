import { connect } from 'react-redux'
import ExpenseSummary from '../components/expenseSummary'

const mapStateToProps = (state) => {
	return {
		total: state.total
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onExpenseClick: (id) => {
			dispatch({
				type: 'TOGGLE_EXPENSE',
				id
			})
		}
	}
}

const ExpenseSummaryContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(ExpenseSummary)

export default ExpenseSummaryContainer
