import React from 'react'
import { connect } from 'react-redux'
import { addBudget } from '../actions/actions'
import CreateBudget from '../components/createBudget'

const mapStateToProps = (state) => {
	return {
		isEditingBudget: state.isEditingBudget
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onAddBudget() {
			dispatch(addBudget())
		}
	}
}

const AddBudgetContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(CreateBudget)

export default AddBudgetContainer
