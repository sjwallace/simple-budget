import { connect } from 'react-redux'
import ExpenseList from '../components/ExpenseList'

const mapStateToProps = (state) => {
	return {
		expenses: state.expenses,
		categories: state.categories,
		subcategories: state.subcategories
	}
}

const ExpenseListContainer = connect(
	mapStateToProps
)(ExpenseList)

export default ExpenseListContainer
